#include <avr/io.h>

#define LED PINB5
#define BTN PIND2

int main() {
    DDRB |= (1<<LED); // Set the direction to be ‘output’
    DDRD &= ~(1<<BTN);
    while(1) {
        if(PIND & (1<<BTN)) {
            PORTB |= (1<<LED); // Set the LED pin to HIGH
        }
        else {
            PORTB &= ~(1<<LED); // Set the LED pin to LOW
        }
    }
}
