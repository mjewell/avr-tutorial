#include <avr/io.h>

#define LED PINB5

int main() {
    DDRB |= (1<<LED); // Set the direction to be ‘output’

    while(1) {
        for(int i=0; i<3; i++) {
            PORTB |= (1<<LED); // Set the LED pin to HIGH
        }
        for(int j=0; j<7; j++) {
            PORTB &= ~(1<<LED); // Set the LED pin to LOW
        }
    }
}
