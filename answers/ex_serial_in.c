#include <avr/io.h>
#include <avr/interrupt.h>
#include "uart.h"
#define UART_BAUD_RATE 9600
#ifndef F_CPU
#define F_CPU 16000000UL
#endif
#define LED PINB5

int main() {
  uart_init(UART_BAUD_SELECT_DOUBLE_SPEED(UART_BAUD_RATE,F_CPU));
  sei();
  unsigned char c;
  DDRB |= (1<<LED); // Set the direction to be ‘output’
  while(1) {
      if(uart_available()) {
          c = uart_getc();
          if(c == '1') {
                PORTB |= (1<<LED); // Set the LED pin to HIGH
          }
          else if(c == '0') {

                PORTB &= ~(1<<LED); // Set the LED pin to HIGH
          }
      }
  }
  return 0;
}
