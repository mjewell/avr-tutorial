#include <avr/io.h>
#include <avr/interrupt.h>
#include "uart.h"
#define UART_BAUD_RATE 9600
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#define LED PINB5


void toggle_led(void)
{
    static char flag = 0;
    if (flag == 0)
    {
        /* turn the LED on */
        PORTB |= (1<<LED);
        flag = 1;
    }
    else
    {
        /* turn the LED off */
        PORTB &= ~(1<<LED);
        flag = 0;
    }

    return;
}

ISR(TIMER0_OVF_vect)
{
    static char counter = 0;
    if(counter == 32) {
        counter = 0;
        toggle_led();
    }
    counter++;
}

void setup_timer(void)
{
   /* select internal clock source, max prescaler factor (1024) */
   TCCR0B |= (1<<CS02)|(1<<CS00);

   /* enable timer overflow interrupt */
   TIMSK0 |= (1<<TOIE0);

   /* enable interrupts */
   sei();
}

int main() {
    DDRB |= (1<<LED); // Set the direction to be ‘output’
    setup_timer();
    while(1) {
    }
    return 0;
}
