
#define LED      PINB5

#include <avr/io.h>

// #include "wifly.h"

/* Start of 7 Segment Code */

// const unsigned char LUT[] = {
//     0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x67   };

// void segment_disp_init()
// {
//   /* initialize 6 pins of port D and 2 pins of port B as output  */
//   DDRB |= _BV(PINB0) | _BV(PINB1);
//   DDRD |= _BV(PIND2) | _BV(PIND3) | _BV(PIND4) | _BV(PIND5) |
// _BV(PIND6) | _BV(PIND7);
//   /* turn off the display, i.e. set all pins high */
//   PORTD = 0xFC;
//   PORTB = 0x03;
// }

// void segment_disp_show(unsigned char i)
// {
//   unsigned char val = LUT[i];
//   /* reset (i.e. set to high) the 2 bits of port B we are using */
//   PORTB |=  0x03;
//   /* mask the received value (bottom 2 bits)
//      and use them to clear the corresponding bits of PORT B
//    */
//   PORTB &= ~(val & 0x03);

//   /* reset (i.e. set to high) the 6 bits of port B we are using */
//   PORTD |=  0xFC;
//   /* mask the received value (the top 6 bits)
//      and use them to clear the corresponding bits of PORT D
//    */
//   PORTD &= ~(val & 0xFC);
// }

// /* End of 7 Segment code */

// /* Start of timer code */

// ISR(TIMER0_OVF_vect)
// {
// }

// void setup_timer(void)
// {
//     select internal clock source, max prescaler factor (1024)
//    TCCR0B |= (1<<CS02)|(1<<CS00);

//    /* enable timer overflow interrupt */
//    TIMSK0 |= (1<<TOIE0);

//    /* enable interrupts */
//    sei();
// }

// /* End of timer code */

// /* Start of ADC code */

// void adc_init(){
//   /* Set up the ADC */

//   ADMUX = 0;

//   /* select AVCC as a reference for the ADC */
//   ADMUX |= (1<<REFS0);

//   /* left-shift for easy 8 bit samples, p. 259 */
//   /* i.e. left adjusted.. */
//   ADMUX |= (1<<ADLAR);

//   ADMUX |= (1<<MUX0) | (1<<MUX2);

//   /* turn off digital input buffer on ADC5, p. 260 */
//   DIDR0 |= (1<<ADC5D);

//   /* ADC enable, auto-trigger enable, start conversions,  p. 258*/
//   ADCSRA |= (1<<ADEN) | (1<<ADATE) | (1<<ADSC);

//   /* set ADC clock prescaler do sample every 128 cycles, p. 264,5 */
//   ADCSRA |= (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);

//   return;
// }

// void pwm_init() {
//   // Fast PWM, 8-bit. TOP is set to 0xFF.
//   // Clear OC1A on compare match (set output to low level)
//   // No prescaler
//   DDRB |= (1<<PINB3);
//   PORTB &= ~(1<<PINB3);
//   TCCR2A = (1 << WGM20) | (1 << WGM21) | (1 << COM2A1);
//   TCCR2B = (1 << CS22);
// }


/* Example 1 */
// #define LED PINB5
// int main() {
//   DDRB |= (1<<LED); // Set up LED
//   while(1) {
//     PORTB |= (1 << LED);
//     _delay_ms(500);
//     PORTB &= ~(1<<LED);
//     _delay_ms(500);
//   }
// }

/* Example 2 */
// #define LED PINB5
// #define BTN PINB4
// int main() {
//   DDRB |= (1<<LED); // Set up LED
//   DDRB &= ~(1<<BTN); // Set up button
//   while(1) {
//     if(PINB & (1<<BTN)) {
//       PORTB |= (1 << LED);
//     }
//     else {
//       PORTB &= ~(1<<LED);
//     }
//   }
//   return 0;
// }

/* Example 3 */
// #include <avr/interrupt.h>
// #include "uart.h"
// #define UART_BAUD_RATE 9600
// #ifndef F_CPU
// #define F_CPU 16000000UL
// #endif

// #define LED PINB5
// #define BTN PINB4
// int main() {
//   DDRB |= (1<<LED); // Set up LED
//   DDRB &= ~(1<<BTN); // Set up button
//   uart_init(UART_BAUD_SELECT_DOUBLE_SPEED(UART_BAUD_RATE,F_CPU));
//   sei();
//   while(1) {
//     if(PINB & (1<<BTN)) {
//       PORTB |= (1 << LED);
//       uart_puts("Hello World\r\n");
//     }
//     else {
//       PORTB &= ~(1<<LED);
//     }
//   }
//   return 0;
// }

/* Example 4 */
#include <avr/interrupt.h>
#include "uart.h"
#define UART_BAUD_RATE 9600
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#define LED PINB5
#define BTN PINB4
int main() {
  DDRB |= (1<<LED); // Set up LED
  DDRB &= ~(1<<BTN); // Set up button
  uart_init(UART_BAUD_SELECT_DOUBLE_SPEED(UART_BAUD_RATE,F_CPU));
  sei();
  while(1) {
    // if(uart_available()) {
    unsigned char c = uart_getc();
    if(c == '1') {
      PORTB |= (1 << LED);
    }
    else if(c == '0') {
      PORTB &= ~(1<<LED);
    }
    // }
  }
  return 0;
}


/* End of ADC code */

// int main (void)
// {
  // // Set up LED
  // DDRB |= (1<<LED);


  // // Set up serial
  // uart_init(UART_BAUD_SELECT_DOUBLE_SPEED(UART_BAUD_RATE,F_CPU));
  // sei();

  // while(1) {
  //   uart_puts("Hello World\r\n");
  //   _delay_ms(1000);
  // }
  // // Set up wifi
  // init_wifly();
  // set_credentials("lapsang");
  // set_host("192.168.2.1", 3000);

  /* Start of ADC code */
  // adc_init();
  // pwm_init();
  // int i = 0;
  // while(1) {
  //   i = ADCH;
  //   uart_puti(i);
  //   uart_puts("\n");
  //   _delay_ms(1000);
  //   // post_data(1, 1, 3);
  //   OCR2A = i;
  // }
  /* Start of PWM code */

  // Fast PWM, 8-bit. TOP is set to 0xFF.
  // Clear OC1A on compare match (set output to low level)
  // No prescaler
  // DDRB |= (1<<PINB3);
  // PORTB &= ~(1<<PINB3);
  // TCCR2A = (1 << WGM20) | (1 << WGM21) | (1 << COM2A1);
  // TCCR2B = (1 << CS22);

  // int value=0;
  // while(1) {
  //   value++;
  //   if(value == 100) {
  //     value = 0;
  //   }
  //   // OCR2A acts as the duty cycle
  //   OCR2A = value;
  //   _delay_ms(30);
  // }
  /* End of PWM code */

  /* Start of serial input code */
  // while(1) {
  //   if(uart_available()) {
  //     unsigned char c = uart_getc();
  //     if(c == '1') {
  //       PORTB |= (1 << LED);
  //     }
  //     else if(c == '0') {
  //       PORTB &= ~(1<<LED);
  //     }
  //   }
  // }
  /* End of serial input code */

  // unsigned char i = 0;
  // unsigned char  val = 0;
  // /* Start of POST code */
  // while(1) {
  //   i = ADCH;
  //   // if timer triggered... or just a delay?
  //   post_data(1, 1, i);
  // }
  // /* End of POST code */

  // /* Start of GET code */
  // while (1) {
  //   if(i == 0) {
  //     val = get_data(1,1);
  //     segment_disp_show(val);
  //   }
  //   _delay_ms(1000);
  // }
  // /* End of GET code */

  // return 0;
// }
