#include <avr/io.h>
#include <avr/interrupt.h>
#include "uart.h"
#define UART_BAUD_RATE 9600
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

int main() {
  uart_init(UART_BAUD_SELECT_DOUBLE_SPEED(UART_BAUD_RATE,F_CPU));
  sei();
  while(1) {
      uart_puts("Hello World\r\n");
  }
  return 0;
}
