#include <stdbool.h>

#ifndef _WIFLY
#define _WIFLY



#define CONN_WAIT 0
#define CONN_OK 1
#define CONN_FAIL 2

void activate_cmd_mode(void);
bool join_net(void);
void sendcmd(char *cmd);
void match_prompt(void);
bool match_modem(const char * str);
char* get_read_line();
int read_modem_line();
bool init_wifly();
void debug(char *str);
int check_connected();
bool set_ssid(char *ssid);
void test_post(double data);
void test_get(double data);
void set_host(char* host, int port);
void post_data(int device, int sensor, int data);
int get_data(int device, int sensor);
void uart_puti(int val);
#endif
