#include <avr/io.h>
#include <avr/interrupt.h>
#include "uart.h"
#define UART_BAUD_RATE 9600
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

volatile unsigned int tick = 0;

ISR(TIMER0_OVF_vect)
{
    tick++;
}

void adc_init(){
  /* Set up the ADC */

  ADMUX = 0;

  /* select AVCC as a reference for the ADC */
  ADMUX |= (1<<REFS0);

  /* left-shift for easy 8 bit samples, p. 259 */
  /* i.e. left adjusted.. */
  ADMUX |= (1<<ADLAR);

  /* Choose the input pin - ADC2 here */
  ADMUX |= (1<<MUX1);

  /* turn off digital input buffer on ADC5, p. 260 */
  DIDR0 |= (1<<ADC2D);

  /* ADC enable, auto-trigger enable, start conversions,  p. 258*/
  ADCSRA |= (1<<ADEN) | (1<<ADATE) | (1<<ADSC);

  /* set ADC clock prescaler do sample every 128 cycles, p. 264,5 */
  ADCSRA |= (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);

  return;
}

void setup_timer(void)
{
   /* select internal clock source, max prescaler factor (1024) */
   TCCR0B |= (1<<CS02)|(1<<CS00);

   /* enable timer overflow interrupt */
   TIMSK0 |= (1<<TOIE0);

   /* enable interrupts */
   sei();
}

int main() {
    adc_init();
    uart_init(UART_BAUD_SELECT_DOUBLE_SPEED(UART_BAUD_RATE,F_CPU));
    setup_timer();
    tick = 0;
    unsigned int i;
    char str[10];
    while(1) {
        if(tick == 50) {
            tick = 0;
            i = ADCH;
            itoa(i, str, 10);
            uart_puts(str);
            uart_puts("\r\n");
        }
    }
    return 0;
}
