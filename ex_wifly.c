
#include "wifly.h"
#include <avr/interrupt.h>
#include "uart.h"
#define UART_BAUD_RATE 9600
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

void delay() {
    unsigned int i,j;
    for(i=0; i<1; i++) {
        for(j=0; j<16000; j++) {
            __asm__ __volatile__ ("nop\n\t");
        }
    }
}

int main() {
    int data = 0;
    uart_init(UART_BAUD_SELECT_DOUBLE_SPEED(UART_BAUD_RATE,F_CPU));
    sei();
    init_wifly();
    set_ssid("lapsang");
    set_host("192.168.2.1", 3000);
    while(1) {
        // Get a value from the server
        data = get_data(1, 1);
        // Post it straight back.
        post_data(1, 1, data);
        delay();
    }
    return 0;
}
