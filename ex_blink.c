#include <avr/io.h>

#define LED PINB5

void delay() {
    unsigned int i,j;
    for(i=0; i<100; i++) {
        for(j=0; j<16000; j++) {
            __asm__ __volatile__ ("nop\n\t");
        }
    }
}

int main() {
    DDRB |= (1<<LED); // Set the direction to be ‘output’
    while(1) {
        PORTB |= (1<<LED); // Set the LED pin to HIGH
        delay();
        PORTB &= ~(1<<LED); // Set the LED pin to LOW
        delay();
    }
}
