
#include <avr/wdt.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <util/delay.h>
#include "uart.h"
#include "wifly.h"
#define LIMIT 500

void _delay(int ms) {
  int i=0;
    for(i=0; i<ms/10; i++) {
        _delay_ms(10);
    }
}

void uart_puti(int val) {
  char str[10];
  itoa(val, str, 10);
  uart_puts(str);
}



char ip_match[5] = "CP=ON";
char fail_match[5] = "AILED";
char buffer[5] = "12345";

char buf[90] = "";

void drain() {
  char c;
  int count = 0;
  while(count < LIMIT) {
    c = 0;
    if(uart_available()) {
      c = uart_getc();
      if(c == '\n') {
        return;
      }
    }
    else {
      _delay_ms(10);
      count++;
    }
  }
}

char* get_read_line() {
  return (char *)buf;
}

int read_modem_line() {
  char c;
  int i=0,count=0;
  while(count < LIMIT) {
    c = 0;
    if(uart_available()) {
      c = uart_getc();
      if(c == '\n') {
        if(i < 90) {
          buf[i] = 0;
        }
        return i;
      }
      if(i < 90) {
        buf[i] = c;
      }
      i++;

    }
    else {
      count++;
      _delay_ms(10);
    }
  }
  return i;
}

// TODO: Refactor this.
char get_clos_val() {
  char c;
  char pre = 0;
  int count=0, i=0;
  char* str = "*CLOS*";
  const char len = strlen(str);
  if (len < 1) {
    return 0;
  }
  while (count < LIMIT) {
    c = 0;
    if(uart_available()) {
      c = uart_getc();
      if (c == str[i]) {
        i += 1;
      }
      else {
        i = 0;
        pre = c;
      }
      if (i == len) {
        return pre;
      }
    }
    else
    {
      count++;
      _delay_ms(1);
    }
  }

  return 0;
}

bool match_modem(const char * str) {
  char c;
  int count=0, i=0;
  const char len = strlen(str);
  if (len < 1) {
    return false;
  }
  while (count < LIMIT) {
    c = 0;
    if(uart_available()) {
      c = uart_getc();
      if (c == str[i]) {
        i += 1;
      }
      else {
        i = 0;
      }
      if (i == len) {
        return true;
      }
    }
    else
    {
      count++;
      _delay_ms(1);
    }
  }

  return false;
}

void sendcmd(char *cmd)
{
  uart_puts(cmd);
}

void activate_cmd_mode(void) {
  drain();
  sendcmd("exit\r\n");
  _delay(100);
  sendcmd("$$$");
  _delay(100);
  if(read_modem_line() == 0) {
    sendcmd("\r\n");
  }
  _delay_ms(200);
  return;
}

void match_prompt(void) {
  match_modem("<4.00>");
}

bool join_net(void) {
  sendcmd("leave\r\n");
  match_prompt();
  sendcmd("join\r\n");
  return match_modem("IP=");
}

bool set_ssid(char *ssid) {
  activate_cmd_mode();
  sendcmd("leave\r\n");
  match_prompt();
  uart_puts("set wlan ssid ");
  uart_puts(ssid);
  uart_puts("\r\n");
  match_prompt();
  sendcmd("set wlan auth 0\r\n");
  match_prompt();
  sendcmd("set wlan join 1\r\n");
  match_prompt();
  sendcmd("save\r\n");
  match_prompt();
  bool joined = join_net();
  match_prompt();
  return joined;
}

void debug(char *str) {
    uart_puts("set ftp user ");
    uart_puts(str);
    uart_puts("\r\n");
    match_prompt();
}

bool init_wifly() {
    uart_flush();
    activate_cmd_mode();
    sendcmd("get ip\r\n");
    return match_modem("IF=UP");
}

bool compare_buffer(char cmp[]) {
  int j=0;
  for(j=0; j<4; j++) {
    if(buffer[j] != cmp[j]) {
      return false;
    }
  }
  return true;
}

void set_host(char* host, int port) {
  // Set server to connect to
  uart_puts("set ip host ");
  uart_puts(host);
  uart_puts("\r\n");
  match_prompt();

  // Set port
  uart_puts("set ip remote ");
  uart_puti(port);
  uart_puts("\r\n");
  match_prompt();

  // Don't send anything by default
  sendcmd("set com remote 0\r\n");
  match_prompt();
}

void post_data(int device, int sensor, int data) {
  activate_cmd_mode();
  sendcmd("open\r\n");
  match_modem("*OPEN*");
  uart_puts("POST /device/");
  uart_puti(device);
  uart_puts("/sensor/");
  uart_puti(sensor);
  uart_puts("/readings");
  uart_puts(" HTTP/1.0\r\n");
  sendcmd("Accept: text/json\r\n");
  sendcmd("Content-Type: application/x-www-form-urlencoded\r\n");
  sendcmd("User-Agent: myIoT/0.1\r\n");
  sendcmd("Content-Length: 7\r\n");
  sendcmd("\r\n");
  uart_puts("value=");
  uart_puti(data);
  uart_puts("\r\n");
  sendcmd("\r\n");
  match_modem("*CLOS*");
}

int get_data(int device, int sensor) {
  activate_cmd_mode();
  sendcmd("open\r\n");
  match_modem("*OPEN*");
  uart_puts("GET /device/");
  uart_puti(device);
  uart_puts("/sensor/");
  uart_puti(sensor);
  uart_puts("/readings");
  uart_puts("\r\n");
  sendcmd("\r\n");
  char c = get_clos_val();
  return ((int)c-48);
}

// CONN_SUCCESS, CONN_FAIL, CONN_WAIT

int check_connected() {
    if(uart_available()) {
      unsigned char c = uart_getc();
      if(c == '\r') {
          //serial_printf("eol %s\r\n", &buffer);
        if(compare_buffer(fail_match)) {
          return CONN_FAIL;
        }
        else if(compare_buffer(ip_match)) {
          return CONN_OK;
        }
        else {
          return CONN_WAIT;
        }
      }
      else
      {
          int i=0;
          for(i=0; i<4; i++) {
              buffer[i] = buffer[i+1];
          }
          buffer[4] = c;
      }
  }
  return CONN_WAIT;
}





